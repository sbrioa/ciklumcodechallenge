# ciklumCodeChallenge

Ciklum Code Challenge


## Getting Started

As mentined in the pre-requisites, project it is done by keeping always in mind the maximun "KEEP IT SIMPLE" so:


- No Spring, just JAX-RS and Jersey, in order to get a lightweight App.
- Front end deployment included in the same .war distributable to be deployed at the same time than the REST API.
- No Database or cache system, only a ConcurrentHashMap to store data and avoid concurrency issues/locks.
- No authentication implemented, (Public API, no need for, no JWT etc..).
- No complex response builder, it is done by Jackson.
- No complex front-end frameworks (no Angular, react, etc...) only CSS, HTML and JavaScript.
- Avoiding libraries imports, no bootstrap , jquery etc... Using design and CSS for partially responsiveness. 


### Prerequisites

Maven, Git, any IDE (Eclipse, IntelliJ), webserver/application server.

## Running the tests

- Junit 5.x has a bug with JerseyTest extension class, evaluating if downgrade to Junit 4.x or to use another Jersey framewook
- CORS enabled by custom filter implementation of ContainerResponseFilter, remember to activate CORS filtering in TOMCAT server for deployment.
- Run Junit tests directly from eclipse or maven command line.


## Deployment

Deploy with Apache Tomcat version 8+. (remember to activate CORS).

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

Standar Maven versioning

## Authors

* **Santiago Brioa Delgado** - *Initial work* 


## License

This project is licensed under the MIT License - 


