'use strict'

let roundsNumber = 0;
const $ = DOMid => document.getElementById(DOMid) 

const url = "http://localhost:8080/ciklum/api"
const getStats = async () => await fetch(`${url}/stats`).then(async res => await res.json()) 
const postNewRound = async () => await fetch(`${url}/newround`,{ method: 'post' }).then(async res => await res.json())
const updateRoundsNumber = () => $('roundsNumber').innerHTML = roundsNumber
const updateGlobalStats = ({totalGames, totalDraws, playerOneWins, playerTwoWins, error}) => {
    if (error) return console.log(error)
    $('playerOneWins').innerHTML = playerOneWins
    $('playerTwoWins').innerHTML = playerTwoWins
    $('totalDraws').innerHTML = totalDraws
    $('totalGames').innerHTML = totalGames
}
const updateUserStats = ({playerOnePick, playerTwoPick, roundWinner, error}) => {
    if (error) return console.log(error)
    roundsNumber++
    updateRoundsNumber()
    const newRow = document.createElement('tr')
    newRow.innerHTML = `<td>${roundsNumber}</td><td>${playerOnePick}</td><td>${playerTwoPick}</td><td>${roundWinner}</td>`
    $('gameTableBody').insertBefore(newRow, $('gameTableBody').firstChild);
}
const resetUserStats = () => {
    roundsNumber = 0
    updateRoundsNumber()
    $('gameTableBody').innerHTML = ''
}
const playButton = async () => {
    $('playButton').disabled = true
    updateUserStats(await postNewRound()) 
    updateGlobalStats(await getStats()) 
    $('playButton').disabled= false
}
const resetButton = async () => {
    $('resetButton').disabled = true
    resetUserStats()    
    updateGlobalStats(await getStats()) 
    $('resetButton').disabled= false
}

window.onload = async () => {
    $('playButton').addEventListener('click', () => playButton())
    $('resetButton').addEventListener('click', () => resetButton())
    updateGlobalStats(await getStats()) 
}