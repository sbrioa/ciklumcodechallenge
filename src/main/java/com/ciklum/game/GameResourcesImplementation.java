package com.ciklum.game;

import java.util.Map;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ciklum.game.model.GameRequest;
import com.ciklum.game.model.GameResponse;
import com.ciklum.game.model.GameTagsEnum;
import com.ciklum.game.model.RoundResponse;
import com.ciklum.game.model.StatsResponse;
import com.ciklum.game.services.GameService;
import com.ciklum.game.utils.GameUtils;

/**
 * Main class holding the entry points for the Rock, Paper, Scissors game.
 */
@Singleton
@Path("/")
public class GameResourcesImplementation {

   private Map<GameTagsEnum, Integer> globalData = GameUtils.initGlobalData();


   @POST
   @Path("newround")
   @Consumes({ MediaType.WILDCARD })
   @Produces(MediaType.APPLICATION_JSON)
   public RoundResponse playSimpleRound() {

      GameRequest request = GameUtils.createGameRequest(globalData);
      GameResponse response = new GameResponse();
      
      new GameService(request, response).run();
      
      return response.getRoundResponse();
   }

   @GET
   @Path("stats")
   @Consumes({ MediaType.WILDCARD })
   @Produces(MediaType.APPLICATION_JSON)
   public StatsResponse retrieveGlobalGameData() {
      return GameUtils.computeGlobalStats(globalData);
   }
}
