package com.ciklum.game.interfaces;

import com.ciklum.game.model.GameTagsEnum;

/**
 * Abstract class to hold players behaviour.
 */
public abstract class GamePlayer {

   protected GameTagsEnum playerPick;

   public abstract GameTagsEnum getPlayerPick();

}
