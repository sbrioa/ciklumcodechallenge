package com.ciklum.game.model;

import com.ciklum.game.interfaces.GamePlayer;

/**
 * This class represent a player with a fixed pick.
 */
public class FixedPickPlayer extends GamePlayer {

   @Override
   public GameTagsEnum getPlayerPick() {
      playerPick = GameTagsEnum.ROCK;
      return playerPick;
   }

}
