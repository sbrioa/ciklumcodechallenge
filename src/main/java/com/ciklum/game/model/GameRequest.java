package com.ciklum.game.model;

import java.util.Map;

import com.ciklum.game.interfaces.GamePlayer;

public class GameRequest {

   private Map<GameTagsEnum, Integer> globalData;

   GamePlayer playerOne;
   GamePlayer playerTwo;

   public GameRequest(GamePlayer playerOne, GamePlayer playerTwo,
	 Map<GameTagsEnum, Integer> globalData) {
      this.playerOne = playerOne;
      this.playerTwo = playerTwo;
      this.globalData = globalData;
   }

   public Map<GameTagsEnum, Integer> getGlobalData() {
      return globalData;
   }

   public void setGlobalData(Map<GameTagsEnum, Integer> globalData) {
      this.globalData = globalData;
   }

   public GamePlayer getPlayerOne() {
      return playerOne;
   }

   public GamePlayer getPlayerTwo() {
      return playerTwo;
   }
}
