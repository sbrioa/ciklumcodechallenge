package com.ciklum.game.model;

/**
 * Response model for a game.
 */
public class GameResponse {

   private RoundResponse roundResponse;

   /**
    * Return the result of a played round.
    * 
    * @return roundResponse
    */
   public RoundResponse getRoundResponse() {
      return roundResponse;
   }

   /**
    * Sets the result of a played round.
    * 
    * @return roundResponse
    */
   public void setRoundResponse(RoundResponse roundResponse) {
      this.roundResponse = roundResponse;
   }

}
