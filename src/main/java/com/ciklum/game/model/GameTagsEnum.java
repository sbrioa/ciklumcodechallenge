package com.ciklum.game.model;

/**
 * Enum holding all the common fields used in the Rock, Paper, Scissors game.
 * 
 */
public enum GameTagsEnum {
   PLAYER_ONE, PLAYER_TWO, DRAW, ROCK, PAPER, SCISSORS, ERROR
}
