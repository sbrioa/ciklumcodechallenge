package com.ciklum.game.model;

import com.ciklum.game.interfaces.GamePlayer;
import com.ciklum.game.utils.GameUtils;

/**
 * This class represent a player with a random pick.
 */
public class RamdomPickPlayer extends GamePlayer {

   @Override
   public GameTagsEnum getPlayerPick() {
      playerPick = GameUtils.generateRamdomPick();
      return playerPick;
   }

}
