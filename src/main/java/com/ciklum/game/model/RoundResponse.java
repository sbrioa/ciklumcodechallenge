package com.ciklum.game.model;

/**
 * Round response model.
 */
public class RoundResponse {

   private GameTagsEnum playerOnePick;

   private GameTagsEnum playerTwoPick;

   private GameTagsEnum roundWinner;

   public RoundResponse(GameTagsEnum playerOnePick, GameTagsEnum playerTwoPick, GameTagsEnum roundWinner) {
      super();
      this.playerOnePick = playerOnePick;
      this.playerTwoPick = playerTwoPick;
      this.roundWinner = roundWinner;
   }

   public GameTagsEnum getPlayerOnePick() {
      return playerOnePick;
   }

   public GameTagsEnum getPlayerTwoPick() {
      return playerTwoPick;
   }

   public GameTagsEnum getRoundWinner() {
      return roundWinner;
   }

   public void setPlayerOnePick(GameTagsEnum playerOnePick) {
      this.playerOnePick = playerOnePick;
   }

   public void setPlayerTwoPick(GameTagsEnum playerTwoPick) {
      this.playerTwoPick = playerTwoPick;
   }

   public void setRoundWinner(GameTagsEnum roundWinner) {
      this.roundWinner = roundWinner;
   }
}
