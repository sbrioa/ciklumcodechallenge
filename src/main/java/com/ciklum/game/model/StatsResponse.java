package com.ciklum.game.model;

public class StatsResponse {

   private Integer totalGamesPlayed;

   private Integer winsByPlayerOne;

   private Integer winsByPlayerTwo;

   private Integer totalGamesDraw;

   public StatsResponse(Integer winsByPlayerOne, Integer winsByPlayerTwo, Integer totalGamesDraw) {
      this.winsByPlayerOne = winsByPlayerOne;
      this.winsByPlayerTwo = winsByPlayerTwo;
      this.totalGamesDraw = totalGamesDraw;
      this.totalGamesPlayed = winsByPlayerOne + winsByPlayerTwo + totalGamesDraw;
   }

   public Integer getPlayerOneWins() {
      return winsByPlayerOne;
   }

   public void setPlayerOneWins(int winsByPlayerOne) {
      this.winsByPlayerOne = winsByPlayerOne;
   }

   public Integer getPlayerTwoWins() {
      return winsByPlayerTwo;
   }

   public void setPlayerTwoWins(int winsByPlayerTwo) {
      this.winsByPlayerTwo = winsByPlayerTwo;
   }

   public Integer getTotalDraws() {
      return totalGamesDraw;
   }

   public void setTotalDraws(int totalGamesDraw) {
      this.totalGamesDraw = totalGamesDraw;
   }

   public Integer getTotalGames() {
      this.totalGamesPlayed = winsByPlayerOne + winsByPlayerTwo + totalGamesDraw;
      return totalGamesPlayed;
   }
}
