package com.ciklum.game.services;

import com.ciklum.game.model.GameRequest;
import com.ciklum.game.model.GameResponse;
import com.ciklum.game.model.GameTagsEnum;
import com.ciklum.game.model.RoundResponse;
import com.ciklum.game.utils.GameUtils;

/**
 * Service in charge of computing the result of a game for the given request.
 */
public class GameService implements Runnable {

   private GameRequest request;
   private GameResponse response;

   public GameService(GameRequest request, GameResponse response) {
      this.request = request;
      this.response = response;
   }

   @Override
   public void run() {

      RoundResponse roundResult = computeRoundWinner(request);

      GameUtils.updateGlobalData(roundResult.getRoundWinner(), request.getGlobalData());

      response.setRoundResponse(roundResult);

   }

   private static RoundResponse computeRoundWinner(GameRequest request) {

      GameTagsEnum playerOnePick = request.getPlayerOne().getPlayerPick();
      GameTagsEnum playerTwoPick = request.getPlayerTwo().getPlayerPick();

      // Check that Player Two always picks ROCK
      if (GameTagsEnum.ROCK.equals(playerTwoPick)) {

	 switch (playerOnePick) {

	 case ROCK:
	    return new RoundResponse(playerOnePick, playerTwoPick, GameTagsEnum.DRAW);

	 case PAPER:
	    return new RoundResponse(playerOnePick, playerTwoPick, GameTagsEnum.PLAYER_ONE);

	 case SCISSORS:
	    return new RoundResponse(playerOnePick, playerTwoPick, GameTagsEnum.PLAYER_TWO);

	 default:
	    break;
	 }

      }
      // If Player Two have not picked ROCK or not picked at all,
      // the above logic it is not valid, then, build error response.
      return new RoundResponse(playerOnePick, playerTwoPick, GameTagsEnum.ERROR);
   }

}
