package com.ciklum.game.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import com.ciklum.game.model.FixedPickPlayer;
import com.ciklum.game.model.GameRequest;
import com.ciklum.game.model.GameTagsEnum;
import com.ciklum.game.model.RamdomPickPlayer;
import com.ciklum.game.model.StatsResponse;

/**
 * Utility class to hold common methods used in the Rock, Paper, Scissors game.
 */
public class GameUtils {

   private GameUtils() {
   }

   /**
    * Initialises the ConcurrentHasMap that we are going to use to store and track
    * the results of all the rounds played globally
    * 
    * @return statsMap
    */
   public static Map<GameTagsEnum, Integer> initGlobalData() {
      ConcurrentHashMap<GameTagsEnum, Integer> statsMap = new ConcurrentHashMap<>();
      statsMap.put(GameTagsEnum.PLAYER_ONE, NumberTags.ZERO);
      statsMap.put(GameTagsEnum.PLAYER_TWO, NumberTags.ZERO);
      statsMap.put(GameTagsEnum.DRAW, NumberTags.ZERO);
      return statsMap;
   }

   /**
    * Return the total WINS per player, total DRAW and the GLOBAL game COUNT.
    * 
    * @return StatsResponse
    */
   public static StatsResponse computeGlobalStats(Map<GameTagsEnum, Integer> globalData) {
      return new StatsResponse(globalData.getOrDefault(GameTagsEnum.PLAYER_ONE, NumberTags.ZERO),
	    globalData.getOrDefault(GameTagsEnum.PLAYER_TWO, NumberTags.ZERO),
	    globalData.getOrDefault(GameTagsEnum.DRAW, NumberTags.ZERO));
   }

   /**
    * Updates the global data stats with the winner of the round if any.
    */
   public static void updateGlobalData(GameTagsEnum toIncrement, Map<GameTagsEnum, Integer> data) {

      switch (toIncrement) {
      case PLAYER_ONE:
	 data.put(GameTagsEnum.PLAYER_ONE, data.get(GameTagsEnum.PLAYER_ONE) + NumberTags.INCREMENT);
	 break;
      case PLAYER_TWO:
	 data.put(GameTagsEnum.PLAYER_TWO, data.get(GameTagsEnum.PLAYER_TWO) + NumberTags.INCREMENT);
	 break;
      case DRAW:
	 data.put(GameTagsEnum.DRAW, data.get(GameTagsEnum.DRAW) + NumberTags.INCREMENT);
	 break;
      default:
	 break;
      }

   }

   /**
    * Generate a random pick that will be set for a player.
    * 
    * @return StatsResponse
    */
   public static GameTagsEnum generateRamdomPick() {
      int random = ThreadLocalRandom.current().nextInt(NumberTags.RANDOM_MIN, NumberTags.RANDOM_MAX);
      if (random < 33) {
	 return GameTagsEnum.PAPER;
      } else if (random <= 66) {
	 return GameTagsEnum.ROCK;
      }
      return GameTagsEnum.SCISSORS;
   }

   /**
    * Creates a request to play a game round.
    * 
    * @return GameRequest
    */
   public static GameRequest createGameRequest(Map<GameTagsEnum, Integer> globalData) {
      return new GameRequest(new RamdomPickPlayer(), new FixedPickPlayer(), globalData);
   }
}
