package com.ciklum.game.utils;

/**
 * Utility class holding numerics tags.
 */
public class NumberTags {

   private NumberTags() {
   }

   public static final Integer INCREMENT = 1;

   public static final Integer ZERO = 0;

   public static final Integer ONE = 1;

   public static final Integer RANDOM_MIN = 0;

   public static final Integer RANDOM_MAX = 100;

   public static final Integer THREE_ITERATIONS = 3;

   public static final Integer RANDOM_ITERATIONS = 30;

   public static final Integer PLAYER_ONE_WINS = 7;

   public static final Integer PLAYER_TWO_WINS = 11;

   public static final Integer TOTAL_DRAWS = 3;

   public static final Integer TOTAL_GAME_ROUNDS = PLAYER_ONE_WINS + PLAYER_TWO_WINS + TOTAL_DRAWS;
}
