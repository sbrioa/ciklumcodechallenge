package com.ciklum.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;

/**
 * Still having problems to test Jersey with an embebbed container in Junit 5,
 * standalone server is working fine.
 *
 */
public class GameResourcesImplementationTest extends JerseyTest {

   private static final int STATUS_200_OK = 200;

   @Override
   public Application configure() {
      enable(TestProperties.LOG_TRAFFIC);
      enable(TestProperties.DUMP_ENTITY);
      return new ResourceConfig(GameResourcesImplementation.class);
   }

   // @Test
   public void shouldPlaySimpleRound() {
      Response response = target("/newround").request().get();
      assertEquals(STATUS_200_OK, response.getStatus());
      assertNotNull(response.getEntity().toString());
   }

   // @Test
   public void shouldRetrieveGlobalGameData() {
      Response output = target("/stats").request().get();
      assertEquals(STATUS_200_OK, output.getStatus());
      assertNotNull(output.getEntity());
   }

}
