package com.ciklum.services;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Objects;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ciklum.game.model.FixedPickPlayer;
import com.ciklum.game.model.GameRequest;
import com.ciklum.game.model.GameResponse;
import com.ciklum.game.model.GameTagsEnum;
import com.ciklum.game.model.RamdomPickPlayer;
import com.ciklum.game.model.RoundResponse;
import com.ciklum.game.services.GameService;
import com.ciklum.game.utils.GameUtils;
import com.ciklum.game.utils.NumberTags;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

   private GameRequest request;

   private GameResponse response;

   private GameService service;

   private Map<GameTagsEnum, Integer> globalData;

   @Test
   public void shouldComputeRoundWinner() {
      // Prepare
      prepareDataForTest();

      // Execute
      service.run();

      // Verify
      assertNotNull(response);

      RoundResponse roundResponse = response.getRoundResponse();
      assertNotNull(roundResponse);
      assertTrue(roundResponse.getPlayerOnePick() instanceof GameTagsEnum);
      assertTrue(roundResponse.getPlayerTwoPick() instanceof GameTagsEnum);
      assertTrue(roundResponse.getRoundWinner() instanceof GameTagsEnum);
      assertNotNull(globalData);
      assertEquals(NumberTags.ONE,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldComputePlayerOneAsRoundWinner() {
      // Prepare
      service = prepareMockedDataForTest(GameTagsEnum.PAPER);

      // Execute
      service.run();

      // Verify
      assertNotNull(response);

      RoundResponse roundResponse = response.getRoundResponse();
      assertNotNull(roundResponse);
      assertTrue(GameTagsEnum.PAPER.equals(roundResponse.getPlayerOnePick()));
      assertTrue(GameTagsEnum.ROCK.equals(roundResponse.getPlayerTwoPick()));
      assertTrue(GameTagsEnum.PLAYER_ONE.equals(roundResponse.getRoundWinner()));
      assertEquals(NumberTags.ONE, globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.ONE,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }
   
   @Test
   public void shouldComputePlayerTwoAsRoundWinner() {
      // Prepare
      service = prepareMockedDataForTest(GameTagsEnum.SCISSORS);

      // Execute
      service.run();

      // Verify
      assertNotNull(response);

      RoundResponse roundResponse = response.getRoundResponse();
      assertNotNull(roundResponse);
      assertTrue(GameTagsEnum.SCISSORS.equals(roundResponse.getPlayerOnePick()));
      assertTrue(GameTagsEnum.ROCK.equals(roundResponse.getPlayerTwoPick()));
      assertTrue(GameTagsEnum.PLAYER_TWO.equals(roundResponse.getRoundWinner()));
      assertEquals(NumberTags.ONE, globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.ONE,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }
   
   @Test
   public void shouldComputeDrawAsRoundResult() {
      // Prepare
      service = prepareMockedDataForTest(GameTagsEnum.ROCK);

      // Execute
      service.run();

      // Verify
      assertNotNull(response);

      RoundResponse roundResponse = response.getRoundResponse();
      assertNotNull(roundResponse);
      assertTrue(GameTagsEnum.ROCK.equals(roundResponse.getPlayerOnePick()));
      assertTrue(GameTagsEnum.ROCK.equals(roundResponse.getPlayerTwoPick()));
      assertTrue(GameTagsEnum.DRAW.equals(roundResponse.getRoundWinner()));
      assertEquals(NumberTags.ONE, globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.ONE,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldComputeErrorAsRoundResult() {
      // Prepare
      service = prepareMockedDataForTest(GameTagsEnum.ERROR);

      // Execute
      service.run();

      // Verify
      assertNotNull(response);

      RoundResponse roundResponse = response.getRoundResponse();
      assertNotNull(roundResponse);
      assertTrue(GameTagsEnum.ERROR.equals(roundResponse.getPlayerOnePick()));
      assertTrue(GameTagsEnum.ROCK.equals(roundResponse.getPlayerTwoPick()));
      assertTrue(GameTagsEnum.ERROR.equals(roundResponse.getRoundWinner()));
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.ZERO,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   private GameService prepareMockedDataForTest(GameTagsEnum playerOnePick) {

      GameRequest request = mock(GameRequest.class);
      RamdomPickPlayer playerOne = mock(RamdomPickPlayer.class);
      FixedPickPlayer playerTwo = new FixedPickPlayer();
      globalData = GameUtils.initGlobalData();
      
      when(request.getPlayerOne()).thenReturn(playerOne);
      when(request.getPlayerOne().getPlayerPick()).thenReturn(playerOnePick);
      when(request.getPlayerTwo()).thenReturn(playerTwo);
      when(request.getGlobalData()).thenReturn(globalData);

      response = new GameResponse();
      return new GameService(request, response);
   }

   private void prepareDataForTest() {
      globalData = GameUtils.initGlobalData();
      request = GameUtils.createGameRequest(globalData);
      response = new GameResponse();
      service = new GameService(request, response);
   }
}
