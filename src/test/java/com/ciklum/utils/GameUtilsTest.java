package com.ciklum.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import com.ciklum.game.model.FixedPickPlayer;
import com.ciklum.game.model.GameRequest;
import com.ciklum.game.model.GameTagsEnum;
import com.ciklum.game.model.RamdomPickPlayer;
import com.ciklum.game.model.StatsResponse;
import com.ciklum.game.utils.GameUtils;
import com.ciklum.game.utils.NumberTags;

public class GameUtilsTest {

   @Test
   public void shouldInitGlobalData() {
      // Execute
      Map<GameTagsEnum, Integer> globalData = GameUtils.initGlobalData();
      // Verify
      assertNotNull(globalData);
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.ZERO, globalData.get(GameTagsEnum.DRAW));
   }

   @Test
   public void shouldComputeGlobalStats() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      StatsResponse response = GameUtils.computeGlobalStats(globalData);
      // Verify
      assertNotNull(response);
      assertEquals(NumberTags.PLAYER_ONE_WINS, response.getPlayerOneWins());
      assertEquals(NumberTags.PLAYER_TWO_WINS, response.getPlayerTwoWins());
      assertEquals(NumberTags.TOTAL_DRAWS, response.getTotalDraws());
      assertEquals((Integer) (NumberTags.TOTAL_GAME_ROUNDS), response.getTotalGames());
   }

   @Test
   public void shouldUpdateGlobalDataForPlayerOne() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      GameUtils.updateGlobalData(GameTagsEnum.PLAYER_ONE, globalData);
      // Verify
      assertNotNull(globalData);
      assertEquals((Integer) (NumberTags.PLAYER_ONE_WINS + NumberTags.INCREMENT),
	    globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.PLAYER_TWO_WINS, globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.TOTAL_DRAWS, globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.TOTAL_GAME_ROUNDS + NumberTags.INCREMENT,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldUpdateGlobalDataForPlayerTwo() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      GameUtils.updateGlobalData(GameTagsEnum.PLAYER_TWO, globalData);
      // Verify
      assertNotNull(globalData);
      assertEquals((Integer) (NumberTags.PLAYER_TWO_WINS + NumberTags.INCREMENT),
	    globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.PLAYER_ONE_WINS, globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.TOTAL_DRAWS, globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.TOTAL_GAME_ROUNDS + NumberTags.INCREMENT,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldUpdateGlobalDataForTotalDraws() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      GameUtils.updateGlobalData(GameTagsEnum.DRAW, globalData);
      // Verify
      assertNotNull(globalData);
      assertEquals((Integer) (NumberTags.TOTAL_DRAWS + NumberTags.INCREMENT), globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.PLAYER_ONE_WINS, globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals(NumberTags.PLAYER_TWO_WINS, globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals(NumberTags.TOTAL_GAME_ROUNDS + NumberTags.INCREMENT,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldUpdateGlobalDataForTotalRoundCount() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      GameUtils.updateGlobalData(GameTagsEnum.PLAYER_ONE, globalData);
      GameUtils.updateGlobalData(GameTagsEnum.PLAYER_TWO, globalData);
      GameUtils.updateGlobalData(GameTagsEnum.DRAW, globalData);
      // Verify
      assertNotNull(globalData);
      assertEquals((Integer) (NumberTags.PLAYER_ONE_WINS + NumberTags.INCREMENT),
	    globalData.get(GameTagsEnum.PLAYER_ONE));
      assertEquals((Integer) (NumberTags.PLAYER_TWO_WINS + NumberTags.INCREMENT),
	    globalData.get(GameTagsEnum.PLAYER_TWO));
      assertEquals((Integer) (NumberTags.TOTAL_DRAWS + NumberTags.INCREMENT), globalData.get(GameTagsEnum.DRAW));
      assertEquals(NumberTags.TOTAL_GAME_ROUNDS + NumberTags.THREE_ITERATIONS,
	    globalData.values().stream()
	    .filter(Objects::nonNull)
	    .mapToInt(Integer::intValue)
	    .sum());
   }

   @Test
   public void shouldGenerateRamdomPick() {
      // Execute
      Object[] arrayRandomPicks = Stream
	    .generate(GameUtils::generateRamdomPick)
	    .limit(NumberTags.RANDOM_ITERATIONS)
	    .toArray();
	
      // Verify
      assertTrue(ArrayUtils.isNotEmpty(arrayRandomPicks));
      assertEquals(NumberTags.THREE_ITERATIONS,
	    Integer.valueOf(Arrays.stream(arrayRandomPicks)
		  .filter(Objects::nonNull)
		  .distinct()
		  .collect(Collectors.counting()).intValue()));
   }

   @Test
   public void shouldCreateGameRequest() {
      // Prepare
      ConcurrentHashMap<GameTagsEnum, Integer> globalData = prepareGlobalData();
      // Execute
      GameRequest request = GameUtils.createGameRequest(globalData);
      // Verify
      assertNotNull(request);
      assertTrue(request.getPlayerOne() instanceof RamdomPickPlayer);
      assertTrue(request.getPlayerTwo() instanceof FixedPickPlayer);
      assertEquals(globalData, request.getGlobalData());
   }

   private static ConcurrentHashMap<GameTagsEnum, Integer> prepareGlobalData() {
      ConcurrentHashMap<GameTagsEnum, Integer> statsMap = new ConcurrentHashMap<GameTagsEnum, Integer>();
      statsMap.put(GameTagsEnum.PLAYER_ONE, NumberTags.PLAYER_ONE_WINS);
      statsMap.put(GameTagsEnum.PLAYER_TWO, NumberTags.PLAYER_TWO_WINS);
      statsMap.put(GameTagsEnum.DRAW, NumberTags.TOTAL_DRAWS);
      return statsMap;
   }
}
